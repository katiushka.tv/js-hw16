let num = +prompt("Enter your number.");

function fib(n) {
    if (n === 0) {
        return n;
    }
    else if (n > 0) {
        let a = 1;
        let b = 1;
        for (let i = 3; i <= n; i++) {
            let c = a + b;
            a = b;
            b = c;
        }
        return b;
    }
    else  {
        let a = 0;
        let b = 1;
        for (let i = -2; i >= n; i--) {
            let c = a - b;
            a = b;
            b = c;
        }
        return b;
    }
}
alert(fib(num));
